<%@page import="java.io.BufferedWriter"%>
<%@page import="java.io.FileWriter"%>
<%@page import="java.io.File"%>
<%@page import="java.nio.file.Files"%>
<%@page
	import="java.util.*,com.onelogin.saml2.Auth,com.onelogin.saml2.settings.Saml2Settings"
	language="java" contentType="application/xhtml+xml"%>
<%
	Auth auth = new Auth();
	Saml2Settings settings = auth.getSettings();
	settings.setSPValidationOnly(true);
	String metadata = settings.getSPMetadata();
	List<String> errors = Saml2Settings.validateMetadata(metadata);
	if (errors.isEmpty()) {
		out.println(metadata);
		
	    File file = new  File("/tmp/metadata.xml");

        if (!file.exists()) 
            file.createNewFile();

        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(metadata);
        bw.close();
		
	} else {
		response.setContentType("text/html; charset=UTF-8");

		for (String error : errors) {
			out.println("<p>" + error + "</p>");
		}
	}
%>