package com.onelogin.saml2.listener;

import java.io.File;
import java.io.FileInputStream;
import java.util.Enumeration;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.onelogin.saml2.bean.AppProperties;

@WebListener
public class InitContext implements ServletContextListener {

	private static final Logger LOGGER = Logger.getLogger(InitContext.class);
	static final String ENV_PROP_FILE_PATH = "onelogin_confpath";

	public void contextDestroyed(ServletContextEvent arg0) {
		try {
			LOGGER.info("Shutdown " + AppProperties.APP_NAME + " successfully!");
		} catch (Exception ex) {
			LOGGER.info("Shutdown " + AppProperties.APP_NAME + " failed!");
		}
	}

	public void contextInitialized(ServletContextEvent arg0) {
		try {

			LOGGER.info("Start read file properties");

			String initFileName = "/oneloginexample.properties";
			String initConfigPath = System.getProperty(ENV_PROP_FILE_PATH);
			String configFileName = initConfigPath + initFileName;

			LOGGER.info("Configuration file: " + configFileName);

			FileInputStream fis = new FileInputStream(new File(configFileName));
			
			AppProperties.getInstance().load(fis);

			Enumeration<?> e = AppProperties.getInstance().propertyNames();
			String key;

			while (e.hasMoreElements()) {
				key = (String) e.nextElement();
				LOGGER.debug(String.format("### key=%s, value=%s ###", key, AppProperties.getInstance().get(key)));
			}

			String filePropS = AppProperties.getInstance().getProperty(AppProperties.LOG_PROP_FILE_PATH);

			File fileProp = new File(filePropS);
			if (!fileProp.exists()) {
				LOGGER.error("Logger properties file (" + filePropS + ") not found!");
			} else {
				PropertyConfigurator.configure(filePropS);
			}
			
			LOGGER.info("Initialized " + AppProperties.APP_NAME + " successfully!");
		} catch (Exception ex) {
			LOGGER.info("Initialized " + AppProperties.APP_NAME + " failed!");
		}
	}

}
