package com.onelogin.saml2;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.onelogin.saml2.exception.Error;
import com.onelogin.saml2.settings.SettingsBuilder;

public final class AuthSettingsBuilder extends com.onelogin.saml2.settings.SettingsBuilder {

	private static final Logger LOGGER = Logger.getLogger(AuthSettingsBuilder.class);
	
	@Override
	public SettingsBuilder fromFile(String propFileName) throws IOException, Error {
		this.loadPropFile(propFileName);
		return this;
	}
	
	private void loadPropFile(String propFileName) throws IOException, Error {

		InputStream inputStream = null;

		try {
			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
			if (inputStream != null) {
				
				Properties prop = new Properties();
				prop.load(inputStream);
				
				this.fromProperties(prop);

				LOGGER.debug("properties file " + propFileName + "loaded succesfully");
			} else {
				String errorMsg = "properties file '" + propFileName + "' not found in the classpath";
				LOGGER.error(errorMsg);
				throw new Error(errorMsg, Error.SETTINGS_FILE_NOT_FOUND);
			}
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (IOException e) {
				LOGGER.warn("properties file '"  + propFileName +  "' not closed properly.");
			}
		}
	}

}
