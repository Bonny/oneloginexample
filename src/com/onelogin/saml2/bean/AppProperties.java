package com.onelogin.saml2.bean;

import java.util.Properties;

public final class AppProperties extends Properties {

	private static final long serialVersionUID = 1L;
	private static final AppProperties instance = new AppProperties();
	
	public static final String APP_NAME = "OneLoginExample";
	
	public static final String LOG_PROP_FILE_PATH = "log.properties.file.path";
	public static final String ONELOGIN_SAML_PROP_FILE_PATH = "onelogin.saml.properties.file.path";
	
	private AppProperties() {
	}

	public static AppProperties getInstance() {
		return instance;
	}
}
